#!/bin/bash

# function asking player to choose rock, paper, scissors
function player {
echo "Choose rock, paper, or scissors?: "
read playerChoice
}

#function to generate computers choice
function computer {
#initialized computerChoice variable
computerChoice=""
#generated random number
randomInt=$(( $RANDOM % 3 + 1 ))
#conditionals that assign choice based on random integers
if [[ $randomInt == 1 ]] 
then computerChoice="rock"

elif [[ $randomInt == 2 ]] 
then computerChoice="paper"

else computerChoice="scissors"
fi

echo "Computer chose $computerChoice."
}

#function that plays game
function game {
# conditionals that calculate and print who won the game
if [[ $playerChoice == "rock" && $computerChoice == "paper" ]]
then echo "Paper covers rock. Computer wins!"

elif [[ $playerChoice == "rock" && $computerChoice == "scissors" ]]
then echo "Rock smashes scissors. Player wins!"

elif [[ $playerChoice == "paper" && $computerChoice == "rock" ]]
then echo "Paper covers rock. Player wins!"

elif [[ $playerChoice == "paper" && $computerChoice == "scissors" ]]
then echo "Scissors cuts paper. Computer wins!"

elif [[ $playerChoice == "scissors" && $computerChoice == "rock" ]]
then echo "Rock smashes scissors. Computer wins!"

elif [[ $playerChoice == "scissors" && $computerChoice == "paper" ]]
then echo "Scissors cuts paper. Player wins!"

elif [[ $playerChoice == $computerChoice ]]
then echo "Draw!"
fi
}

player
computer
game
